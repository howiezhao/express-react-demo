# Express React Demo

![Node Version](https://img.shields.io/badge/Node-v12.18.3-blue)
![PostgreSQL Version](https://img.shields.io/badge/PostgreSQL-v12-blue)
![Express Version](https://img.shields.io/badge/Express-v4.16.1-blue)
![Sequelize Version](https://img.shields.io/badge/Sequelize-v6.3.5-blue)
![React Version](https://img.shields.io/badge/React-v16.13.1-blue)
![Ant Design Version](https://img.shields.io/badge/Ant%20Design-v4.6.5-blue)

Server: Express + Sequelize

Client: React + Ant Design

## Use Docker(Recommend)

### Run

1. `docker-compose up`

## Use Traditional Way

### Prerequisites

1. Install PostgreSQL
2. Install dependencies:

    `npm install --prefix server`
    
    `npm install --prefix client`
3. Create Database: `cd server && npx sequelize-cli db:create`
4. Running Migrations: `cd server && npx sequelize-cli db:migrate`
5. Running Seeds: `cd server && npx sequelize-cli db:seed:all`

### Run

1. Running Server: `cd server && npm run start`
2. Running Client: `cd client && npm run start`

## TODO

- [ ] refactor(server): use dotenv, in ./server/.env
- [ ] refactor(server): update sequelize seeders
- [ ] refactor: optimize directory structure, like django apps, update .sequelizerc
- [ ] chore: add docker support
- [ ] docs: add swagger api and jsdoc
- [ ] style: add eslint
- [ ] test: add unit test and e2e test
