import React from 'react';
import {
  BrowserRouter as Router,
  Route
} from "react-router-dom";
import './App.css';
import Modify from "./components/modify";
import Detail from "./components/detail";

function App() {
  return (
    <Router>
      <Route path="/" exact component={Detail} />
      <Route path="/modify" component={Modify} />
    </Router>
  );
}

export default App;
