import React from 'react';
import {
  Link
} from "react-router-dom";
import { Table, Space, Popconfirm, message, Button } from 'antd';
import dayjs from 'dayjs';

class Detail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      categoriesApi: 'http://localhost:3000/api/v1/categories/',
      date: {}
    };
    this.columns = [
      {
        title: 'Name',
        dataIndex: 'name',
        key: 'name',
      },
      {
        title: 'Type',
        dataIndex: 'type',
        key: 'type',
      },
      {
        title: 'Description',
        dataIndex: 'description',
        key: 'description',
      },
      {
        title: 'Is Online?',
        key: 'isOnline',
        dataIndex: 'isOnline',
        render: isOnline => isOnline.toString(),
      },
      {
        title: 'Products Count',
        key: 'ProductsCount',
        dataIndex: 'ProductsCount',
      },
      {
        title: 'Created Date',
        key: 'createdAt',
        dataIndex: 'createdAt',
        render: createdAt => dayjs(createdAt).format('YYYY-MM-DD HH:mm:ss'),
      },
      {
        title: 'Action',
        key: 'action',
        render: (text, record) => (
          <Space size="middle">
            <Link
              to={{
                pathname: "/modify",
                state: {
                  mode: 'update',
                  id: record.id,
                  name: record.name,
                  type: record.type,
                  description: record.description,
                  isOnline: record.isOnline.toString()
                }
              }}>
              Update {record.name}
            </Link>
            <Popconfirm
              title="Are you sure delete this task?"
              onConfirm={() => this.confirm(record.id)}
              onCancel={this.cancel}
              okText="Yes"
              cancelText="No"
            >
              <a href="#">Delete</a>
            </Popconfirm>
          </Space>
        ),
      },
    ];
  }

  componentDidMount() {
    this.fetchData();
  }

  fetchData() {
    fetch(this.state.categoriesApi)
      .then(res => res.json())
      // .then(data => console.log(data))
      .then(data => this.setState({ data: data }))
  }

  confirm(id) {
    fetch(this.state.categoriesApi + id, {
      method: 'DELETE',
    })
      .then(res => res.json())
      .then(() => this.fetchData());
    message.success('Successfully deleted');
  }

  cancel(e) {
    console.log(e);
    message.error('Cancel delete');
  }

  render() {
    return (
      <div>
        <Button type="primary">
          <Link to={{
            pathname: '/modify',
            state: {
              mode: 'create'
            }
          }}>
            Create
          </Link>
        </Button>
        <Table columns={this.columns} dataSource={this.state.data} />
      </div>
    );
  }
}

export default Detail;
