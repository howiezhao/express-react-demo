import React from 'react';
import { Form, Input, Button, Select, Radio } from 'antd';

const { Option } = Select;
const layout = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 16,
  },
};
const tailLayout = {
  wrapperCol: {
    offset: 8,
    span: 16,
  },
};

const Modify = (props) => {
  const categoriesApi = 'http://localhost:3000/api/v1/categories/';

  const onFinish = (values) => {
    console.log('Success:', values);
    if (props.history.location.state.mode === 'create') {
      fetch(categoriesApi, {
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify(values),
      }).then(res => res.json())
        .then(data => console.log(data))
        .then(() => props.history.goBack());
    } else if (props.history.location.state.mode === 'update') {
      fetch(categoriesApi + props.history.location.state.id, {
        method: 'PUT',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify(values),
      }).then(res => res.json())
        .then(data => console.log(data))
        .then(() => props.history.goBack());
    }
  };

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };

  return (
    <Form
      {...layout}
      name="basic"
      initialValues={props.history.location.state}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
    >
      <Form.Item
        label="Name"
        name="name"
        rules={[
          {
            required: true,
          },
          {
            min: 10,
            max: 200,
          }
        ]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        label="Type"
        name="type"
        rules={[
          {
            required: true,
          },
        ]}
      >
        <Select
          placeholder="Select a option"
          allowClear
        >
          <Option value="test">test</Option>
          <Option value="dev">dev</Option>
          <Option value="prod">prod</Option>
        </Select>
      </Form.Item>

      <Form.Item
        label="Description"
        name="description"
      >
        <Input />
      </Form.Item>

      <Form.Item
        label="Is Online?"
        name="isOnline"
        rules={[
          {
            required: true,
          },
        ]}
      >
        <Radio.Group>
          <Radio value="true">True</Radio>
          <Radio value="false">False</Radio>
        </Radio.Group>
      </Form.Item>

      <Form.Item {...tailLayout}>
        <Button type="primary" htmlType="submit">
          Submit
        </Button>
      </Form.Item>
    </Form>
  );
};

export default Modify;
