const db = require('../models')

exports.read = async (req, res) => {

  const categories = await db.Category.findAll({
    attributes: ['id', 'name', 'type', 'description', 'isOnline', 'createdAt',
      [db.sequelize.fn('COUNT', db.sequelize.col('Products')), 'ProductsCount']
    ],
    include: [
      {
        model: db.Product,
        as: 'Products',
        attributes: []
      }
    ],
    group: ['Category.id']
  });

  res.json(categories);
}

exports.create = async (req, res) => {
  if (req.body) {
    await db.Category.create(req.body)
  }
  res.json({code: 'create'})
}

exports.update = async (req, res) => {
  if (req.body) {
    await db.Category.update(
      req.body, {
        where: {
          id: req.params.id
        }
      })
  }
  res.json(req.body.name);
}

exports.delete = async (req, res) => {
  await db.Category.destroy({
    where: {
      id: req.params.id
    }
  });
  res.json({msg: 'success'})
}
