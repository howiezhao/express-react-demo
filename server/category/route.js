const express = require('express');
const router = express.Router();
const categoryController = require('./controller')

/* Read */
router.get('/', categoryController.read);
/* Update */
router.put('/:id', categoryController.update);
/* Create */
router.post('/', categoryController.create);
/* Delete */
router.delete('/:id', categoryController.delete);

module.exports = router;
