'use strict';
const {
  Model
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class Category extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Category.hasMany(models.Product, {as: 'Products'})
    }
  }
  Category.init({
    name: {
      type: DataTypes.STRING,
      validate: {
        len: [10, 200]
      }
    },
    type: DataTypes.ENUM('test', 'dev', 'prod'),
    description: DataTypes.TEXT,
    isOnline: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'Category',
  });
  return Category;
};
