const db = require('../models')

exports.index = async (req, res) => {

  const products = await db.Product.findAll();

  res.json(products);
}
